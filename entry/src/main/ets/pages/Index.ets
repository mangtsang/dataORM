/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

import { Property } from '@ohos/dataorm';
import { DaoSession } from '@ohos/dataorm'
import { BaseDao } from '@ohos/dataorm'
import { Query } from '@ohos/dataorm'
import { QueryBuilder } from '@ohos/dataorm'
import { inquiry } from '@ohos/dataorm'
import { TableAction } from '@ohos/dataorm'
import { OnTableChangedListener } from '@ohos/dataorm'
import { Toolbar } from './toolbar'
import { Note } from './Note'
import { NoteType } from './NoteType'

@Entry
@Component
struct Index {
  @State arr: Array<Note> = new Array<Note>();
  @State editFlag: boolean = false
  @State noteText: string = ''
  private daoSession: DaoSession;
  private noteDao: BaseDao<Note, number>;
  private notesQuery: Query<Note>;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Toolbar({ title: 'dataORM', isBack: true })
      Flex() {
        // @ts-ignore
        TextInput({ placeholder: 'Enter new note', text: this.historyInput })
          .type(InputType.Normal)
          .placeholderColor(Color.Gray)
          .placeholderFont({ size: 20, weight: 2 })
          .enterKeyType(EnterKeyType.Search)
          .caretColor(Color.Green)
          .layoutWeight(3)
          .height(45)
          .borderRadius('0px')
          .backgroundColor(Color.White)
          .onChange((value: string) => {
            this.noteText = value
          })
        Button('ADD')
          .fontSize(13)
          .fontWeight(FontWeight.Bold)
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            this.addNote()
          })
        Button('Query')
          .fontSize(13)
          .fontWeight(FontWeight.Bold)
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            this.query()
          })
      }
      .margin({ top: 12 })

      Flex() {
        Button('Delete bbb and Query')
          .height(45)
          .fontSize(12)
          .padding({ top: 8, left: 8 })
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            this.deleteNotes()
          })
        Button('Query aaa bbb ccc')
          .height(45)
          .fontSize(12)
          .padding({ top: 8 })
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            this.selectWhere()
          })
      }
      .margin({ top: 12 })

      Flex() {
        Button('Load(id 2的缓存)')
          .height(45)
          .fontSize(12)
          .padding({ top: 1, left: 8 })
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            this.loadId2();
          })
        Button('save(没主键时新增)')
          .height(45)
          .fontSize(12)
          .padding({ top: 1 })
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            let note = new Note();
            note.setText("Note_a");
            this.noteDao.save(note)
          })
      }.margin({ top: 5 })

      Flex() {
        Button('flexQuery')
          .height(45)
          .fontSize(12)
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            new inquiry().from(Note)
              .query(Note)
              .then((data) => {
                if (data)
                  this.arr = data;
                else
                  this.arr = [];
              })
          })
        Button('query id 2')
          .height(45)
          .fontSize(12)
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            new inquiry().from(Note)
              .eq("ID", 2)
              .querySingle(Note)
              .then((data) => {
                if (data)
                  this.arr = data;
                else
                  this.arr = [];
              })
          })
        Button('query aaa')
          .height(45)
          .fontSize(12)
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            new inquiry().from(Note)
              .eq("TEXT", "aaa")
              .querySingle(Note)
              .then((data) => {
                if (data)
                  this.arr = data;
                else
                  this.arr = [];
              })
          })
      }.padding({ top: 4, left: 10 })


      List({ space: 20, initialIndex: 0 }) {
        ForEach(this.arr, (item) => {
          ListItem() {
            Flex({ direction: FlexDirection.Row, wrap: FlexWrap.Wrap }) {
              Text('' + item.text)
                .fontSize(13)
              Button("删除").onClick(() => {
                this.noteDao.deleteByKey(item.id)
              })
              Button("修改").onClick(() => {
                item.setText(this.noteText)
                this.noteDao.update(item)
              })
              Button('save')
                .onClick(() => {
                  item.setText(this.noteText)
                  this.noteDao.save(item);
                })
              Button('refresh')
                .onClick(() => {
                  item.setText(this.noteText)
                  this.noteDao.refresh(item);
                  var that = this
                  this.arr = [];
                  setTimeout(function () {
                    that.updateNotes();
                  }, 100)
                })
              Button('refresh前修改')
                .onClick(() => {
                  this.refreshUpdate(item.id)
                })
            }.width('100%')
          }
        })
      }
      .listDirection(Axis.Vertical) // 排列方向
      .divider({ strokeWidth: 2, color: 0xFFFFFF, startMargin: 20, endMargin: 20 }) // 每行之间的分界线
      .edgeEffect(EdgeEffect.None) // 滑动到边缘无效果
      .chainAnimation(false) // 联动特效关闭
      .onScrollIndex((firstIndex: number, lastIndex: number) => {
      })
      .width('90%')
    }
    .width('100%')
    .height('100%')
  }

  refreshUpdate(id: number) {
    let data = new Array<Note>();
    let tmpArr = this.arr;
    this.arr = [];
    for (let i = 0;i < tmpArr.length; i++) {
      if (tmpArr[i].id == id) {
        tmpArr[i].text = tmpArr[i].text + "--2"
      }
      data[i] = tmpArr[i]
    }
    let that = this;
    setTimeout(() => {
      that.arr = data;
    }, 100)
  }

  addNote() {
    let date = new Date()
    let comment = "Added on " + date.toLocaleString();
    let note = new Note();
    note.setText(this.noteText);
    note.setComment(comment);
    note.setDate(new Date());
    note.setType(NoteType[NoteType.TEXT]);
    this.noteDao.insert(note);
  }

  async updateNotes() {
    this.arr = await this.notesQuery.list();
  }

  async query() {

    //方式一
    let properties = globalThis.entityCls[Note.name];
    let query = this.noteDao.queryBuilder().orderAsc(properties['text']).buildCursor();
    // @ts-ignore
    let cursor: ResultSet = await query.query();

    //    //方式二 通过原始sql语句查询
    //    let cursor = await this.noteDao.rawQuery("SELECT * FROM NOTE");

    let a = this.noteDao.convertCursor2Entity(cursor);
    if (!a) a = [];
    this.arr = a;
  }

  async deleteNotes() {
    let properties = globalThis.entityCls[Note.name];
    let deleteQuery = this.noteDao.queryBuilder().where(properties['text'].eq("bbb"))
      .buildDelete();
    deleteQuery.executeDeleteWithoutDetachingEntities()
  }

  async selectWhere() {
    let queryBuilder: QueryBuilder<Note> = this.noteDao.queryBuilder();
    let properties = globalThis.entityCls[Note.name];
    queryBuilder.whereOr(properties['text'].eq("aaa"), properties['text'].eq("bbb"), properties['text'].eq("ccc"));
    let aa: Array<Note> = await queryBuilder.list();
    if (!aa) aa = [];
    this.arr = aa;
  }

  public aboutToAppear() {
    this.getAppData();
  }

  getAppData() {
    this.daoSession = globalThis.daoSession;

    this.noteDao = this.daoSession.getBaseDao(Note);
    /*
     *添加监听
     */
    this.noteDao.addTableChangedListener(this.tabListener())
    let properties: Property[] = globalThis.entityCls[Note.name];

    this.notesQuery = this.noteDao.queryBuilder().orderAsc(properties['text']).build();
  }

  onBackPress() {
    /**
     * 移除监听
     */
    this.noteDao.removeTableChangedListener();

  }

  tabListener(): OnTableChangedListener<any> {
    let that = this;
    return {
      async onTableChanged(t: any, action: TableAction) {
        if (action == TableAction.INSERT) {
          console.info('--------insert--------')
          await that.updateNotes();
        } else if (action == TableAction.UPDATE) {
          console.info('--------edit--------')
          await that.updateNotes();
        } else if (action == TableAction.DELETE) {
          console.info('--------delete--------')
          await that.updateNotes();
        } else if (action == TableAction.QUERY) {
          console.info('--------query-------- any:' + JSON.stringify(t))
        }
      }
    }
  }

  async loadId2() {
    let tmp = await this.noteDao.load(2);
    if (tmp) {
      let a = [tmp];
      this.arr = a;
    } else {
      this.arr = [];
    }

  }
}